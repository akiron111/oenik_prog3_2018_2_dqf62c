# OENIK_PROG3_2018_2_DQF62C

Könyvkiadó adatbázisának táblái:

• BOOK (ISBN, AUTHOR_ID, WORDCOUNT, ADDRESS(TITLE))
• AUTHOR (AUTHOR_ID, NAME, BIRTH_DATE, ADDRESS, ACCOUNTNO, EMAIL)
• PRESS (PRESS_ID, PRESSNAME, EMAIL)
• PRESS_DATA (PRESS_ID, ISBN, PAGENO, COPYNO, PAPERWEIGHT)
• AUTHOR_ID: idegen kulcs a Szerző tábla elsődleges kulcsa
• ISBN: idegen kulcs, a Könyv tábla elsődleges kulcsa
• PRESS_ID: idegen kulcs, a Nyomda tábla elsődleges kulcsa

