var class_book_publisher_1_1_repository_1_1_repository =
[
    [ "Repository", "class_book_publisher_1_1_repository_1_1_repository.html#a6edff0dd7e3b79cdfee73dacd0fe09f5", null ],
    [ "Delete", "class_book_publisher_1_1_repository_1_1_repository.html#aa5b5942e36d205028b711d0e5b8bfa98", null ],
    [ "GetAllAuthor", "class_book_publisher_1_1_repository_1_1_repository.html#a5ab3d0e8ded5765c008d0384bdd06dc8", null ],
    [ "GetAllBook", "class_book_publisher_1_1_repository_1_1_repository.html#a51c1bbdfb5c92dfa8ffc0f02eb604d79", null ],
    [ "GetAllPress", "class_book_publisher_1_1_repository_1_1_repository.html#a50a5c8341fd184e1045d302d29e484b3", null ],
    [ "GetAllPressData", "class_book_publisher_1_1_repository_1_1_repository.html#a668b328b6b5e615e773cffde895837a4", null ],
    [ "Insert", "class_book_publisher_1_1_repository_1_1_repository.html#a0b8471251b86058476a5ee22aef11c01", null ],
    [ "Update", "class_book_publisher_1_1_repository_1_1_repository.html#adc79ef476f2e88d37b07053420b995ca", null ]
];