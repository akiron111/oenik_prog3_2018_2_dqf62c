var interface_book_publisher_1_1_logic_1_1_i_logic =
[
    [ "Delete", "interface_book_publisher_1_1_logic_1_1_i_logic.html#a294f469f51acde42dfd72c72b021f615", null ],
    [ "GetAllAuthor", "interface_book_publisher_1_1_logic_1_1_i_logic.html#af50c5003c47a5eaa5bcd0e053525f5f8", null ],
    [ "GetAllBook", "interface_book_publisher_1_1_logic_1_1_i_logic.html#a6a3b349df68e30fb172956aa8c430473", null ],
    [ "GetAllPress", "interface_book_publisher_1_1_logic_1_1_i_logic.html#a855f631b17caca575056f7985881f338", null ],
    [ "GetAllPressData", "interface_book_publisher_1_1_logic_1_1_i_logic.html#ae1f5142d6398589abb2a6361e3bcba74", null ],
    [ "Insert", "interface_book_publisher_1_1_logic_1_1_i_logic.html#ae86d99822d1c770ea7590dba9996e7a6", null ],
    [ "Update", "interface_book_publisher_1_1_logic_1_1_i_logic.html#a84d8b4b3f1f029b92229fd5ca6894c46", null ]
];