var class_book_publisher_1_1_logic_1_1_tests_1_1_tester =
[
    [ "GetAllAuthorTest", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#ae206ea954611315e374ca95a6ceca14f", null ],
    [ "GetAllBookTest", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#aa900c8277f5bd95b0df70f77f5793a05", null ],
    [ "GetAllDataTest", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#aa59b2fc13e5a92d6b51f78eecd3b4d92", null ],
    [ "GetAllPressTest", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#aca8fb67366b9de35ec55197281f24ec6", null ],
    [ "SetupLogic", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a4d9ac14f17531f41192722f69dd9686b", null ],
    [ "TestDeleteAuthor", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a3c4f7eecae19ba60413cc90af2cfd080", null ],
    [ "TestDeleteBook", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#ad9d703f5826cc6004640ebc4ea123c24", null ],
    [ "TestDeletePress", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a3c09a5487adad1c36d7430f362f411ae", null ],
    [ "TestDeletePressData", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#af223e65d581346126af06c5d21aeffbb", null ],
    [ "TestInsertAuthor", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a016811c4869276ef0989d9449e0b11cc", null ],
    [ "TestInsertBook", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a34f93dae08be58a7bd9cc1495c9f14a3", null ],
    [ "TestInsertData", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a7626bb049a4b034f689015d793ae0ae8", null ],
    [ "TestInsertPress", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a9faf4f636c7382bba88a163cb3cbc3e0", null ],
    [ "TestNonCRUD1", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#aad033f486a01cf33b80023e99d391d71", null ],
    [ "TestNonCRUD2", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a1d6c0fb2e99fdbc138ddc9a1dbf272c2", null ],
    [ "TestNonCRUD3", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a22390931565de266b60a8c05f1294cc3", null ]
];