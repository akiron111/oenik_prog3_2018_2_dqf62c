var class_book_publisher_1_1_data_1_1_book_publisher_d_b_entities1 =
[
    [ "BookPublisherDBEntities1", "class_book_publisher_1_1_data_1_1_book_publisher_d_b_entities1.html#ae0ee5ee4e916350300f80cb7060f8edd", null ],
    [ "OnModelCreating", "class_book_publisher_1_1_data_1_1_book_publisher_d_b_entities1.html#aa0c510e51770ab55b213abd186a9eedf", null ],
    [ "AUTHOR", "class_book_publisher_1_1_data_1_1_book_publisher_d_b_entities1.html#aa13304ee84f6281a97a3029eb3b61c8a", null ],
    [ "BOOK", "class_book_publisher_1_1_data_1_1_book_publisher_d_b_entities1.html#a15291dfcd6476cc89de09adc9c0fa932", null ],
    [ "PRESS", "class_book_publisher_1_1_data_1_1_book_publisher_d_b_entities1.html#a48cfe3f4ea98c6d7679efec2506d30c8", null ],
    [ "PRESS_DATA", "class_book_publisher_1_1_data_1_1_book_publisher_d_b_entities1.html#a4e79e8f1e8ebb10bd206bcd2320ed8df", null ]
];