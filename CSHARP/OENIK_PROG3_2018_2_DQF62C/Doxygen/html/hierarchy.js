var hierarchy =
[
    [ "BookPublisher.Data.AUTHOR", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r.html", null ],
    [ "BookPublisher.Data.BOOK", "class_book_publisher_1_1_data_1_1_b_o_o_k.html", null ],
    [ "DbContext", null, [
      [ "BookPublisher.Data.BookPublisherDBEntities1", "class_book_publisher_1_1_data_1_1_book_publisher_d_b_entities1.html", null ]
    ] ],
    [ "BookPublisher.Logic.ILogic", "interface_book_publisher_1_1_logic_1_1_i_logic.html", [
      [ "BookPublisher.Logic.Logic", "class_book_publisher_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "BookPublisher.Repository.IRepository", "interface_book_publisher_1_1_repository_1_1_i_repository.html", [
      [ "BookPublisher.Repository.Repository", "class_book_publisher_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "OENIK_PROG3_2018_1_DQF62C.Menu", "class_o_e_n_i_k___p_r_o_g3__2018__1___d_q_f62_c_1_1_menu.html", null ],
    [ "BookPublisher.Data.PRESS", "class_book_publisher_1_1_data_1_1_p_r_e_s_s.html", null ],
    [ "BookPublisher.Data.PRESS_DATA", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a.html", null ],
    [ "OENIK_PROG3_2018_1_DQF62C.Program", "class_o_e_n_i_k___p_r_o_g3__2018__1___d_q_f62_c_1_1_program.html", null ],
    [ "BookPublisher.Logic.Tests.Tester", "class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html", null ]
];