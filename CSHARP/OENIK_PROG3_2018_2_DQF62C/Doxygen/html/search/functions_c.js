var searchData=
[
  ['testdeleteauthor',['TestDeleteAuthor',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a3c4f7eecae19ba60413cc90af2cfd080',1,'BookPublisher::Logic::Tests::Tester']]],
  ['testdeletebook',['TestDeleteBook',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#ad9d703f5826cc6004640ebc4ea123c24',1,'BookPublisher::Logic::Tests::Tester']]],
  ['testdeletepress',['TestDeletePress',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a3c09a5487adad1c36d7430f362f411ae',1,'BookPublisher::Logic::Tests::Tester']]],
  ['testdeletepressdata',['TestDeletePressData',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#af223e65d581346126af06c5d21aeffbb',1,'BookPublisher::Logic::Tests::Tester']]],
  ['testinsertauthor',['TestInsertAuthor',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a016811c4869276ef0989d9449e0b11cc',1,'BookPublisher::Logic::Tests::Tester']]],
  ['testinsertbook',['TestInsertBook',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a34f93dae08be58a7bd9cc1495c9f14a3',1,'BookPublisher::Logic::Tests::Tester']]],
  ['testinsertdata',['TestInsertData',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a7626bb049a4b034f689015d793ae0ae8',1,'BookPublisher::Logic::Tests::Tester']]],
  ['testinsertpress',['TestInsertPress',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a9faf4f636c7382bba88a163cb3cbc3e0',1,'BookPublisher::Logic::Tests::Tester']]],
  ['testnoncrud1',['TestNonCRUD1',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#aad033f486a01cf33b80023e99d391d71',1,'BookPublisher::Logic::Tests::Tester']]],
  ['testnoncrud2',['TestNonCRUD2',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a1d6c0fb2e99fdbc138ddc9a1dbf272c2',1,'BookPublisher::Logic::Tests::Tester']]],
  ['testnoncrud3',['TestNonCRUD3',['../class_book_publisher_1_1_logic_1_1_tests_1_1_tester.html#a22390931565de266b60a8c05f1294cc3',1,'BookPublisher::Logic::Tests::Tester']]]
];
