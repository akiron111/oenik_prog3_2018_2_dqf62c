var class_book_publisher_1_1_data_1_1_b_o_o_k =
[
    [ "BOOK", "class_book_publisher_1_1_data_1_1_b_o_o_k.html#acf7ffb7ccc8b2eca002062cdb7b317b7", null ],
    [ "ADDRESS", "class_book_publisher_1_1_data_1_1_b_o_o_k.html#afeac17159a053a4a327203ed90612d59", null ],
    [ "AUTHOR", "class_book_publisher_1_1_data_1_1_b_o_o_k.html#a008a7e56d85c31539f60c30ace9d7299", null ],
    [ "AUTHOR_ID", "class_book_publisher_1_1_data_1_1_b_o_o_k.html#a2e034eca38324a8f2d76ba534e42ea69", null ],
    [ "ISBN", "class_book_publisher_1_1_data_1_1_b_o_o_k.html#a4903602e326f2ef3035ecc58e6dee5e9", null ],
    [ "PRESS_DATA", "class_book_publisher_1_1_data_1_1_b_o_o_k.html#aed4a0559411b40ffcb7b1609524e2f53", null ],
    [ "WORDCOUNT", "class_book_publisher_1_1_data_1_1_b_o_o_k.html#a4c68505d28bc5e9f73062bee08f3da9a", null ]
];