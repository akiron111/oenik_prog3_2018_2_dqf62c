var class_book_publisher_1_1_data_1_1_a_u_t_h_o_r =
[
    [ "AUTHOR", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r.html#ac0cb03a2ed254959ba6f47c21f0b11bb", null ],
    [ "ACCOUNTNO", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r.html#a87120be5e034085fb0459cecb0331c70", null ],
    [ "ADDRESS", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r.html#a8ba59457116217a3068cfa0242fe6043", null ],
    [ "AUTHOR_ID", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r.html#a6da5ec063dfb8d901e0179e1a9fce1b4", null ],
    [ "BIRTHDATE", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r.html#a29e90d6f1a99c3c90160890a35bff123", null ],
    [ "BOOK", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r.html#a738f2fd5d2fc2aa0d81b667575d079c8", null ],
    [ "EMAIL", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r.html#aec9610912312b31449dcd27f67c182f7", null ],
    [ "NAME", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r.html#ae946059e09768ba465be6a224fbeff07", null ]
];