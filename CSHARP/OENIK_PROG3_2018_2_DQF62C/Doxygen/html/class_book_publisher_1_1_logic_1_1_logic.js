var class_book_publisher_1_1_logic_1_1_logic =
[
    [ "Logic", "class_book_publisher_1_1_logic_1_1_logic.html#a0be07423cac631db6101d09b28d751cd", null ],
    [ "Logic", "class_book_publisher_1_1_logic_1_1_logic.html#aaf4c2002f8b235de812bb8fd748f17b2", null ],
    [ "BooksFromPresses", "class_book_publisher_1_1_logic_1_1_logic.html#a55fb2bd8deca072792c94e25862076d3", null ],
    [ "Delete", "class_book_publisher_1_1_logic_1_1_logic.html#a98b97bef295b57e1dd5222dd0da26cf0", null ],
    [ "GetAllAuthor", "class_book_publisher_1_1_logic_1_1_logic.html#a0eb26bdf3c304d387934a53833a278e9", null ],
    [ "GetAllBook", "class_book_publisher_1_1_logic_1_1_logic.html#aaa865d61ec32fe804f2d36fb8b458cba", null ],
    [ "GetAllPress", "class_book_publisher_1_1_logic_1_1_logic.html#a1827ba1793ad754233ff7f74bba6c86f", null ],
    [ "GetAllPressData", "class_book_publisher_1_1_logic_1_1_logic.html#a739688b44565412814868a5cf11cf09b", null ],
    [ "Insert", "class_book_publisher_1_1_logic_1_1_logic.html#a6d40c2148d3b9b65936719fea81981b1", null ],
    [ "MostBooks", "class_book_publisher_1_1_logic_1_1_logic.html#aa87a69babb6eb38fd09d1cb03445a0ad", null ],
    [ "PressesWithBooks", "class_book_publisher_1_1_logic_1_1_logic.html#a3cebde5ecde9a8452c00e150fe8a7915", null ],
    [ "Update", "class_book_publisher_1_1_logic_1_1_logic.html#afd03117519063a0cb74e3f8d0b5433f2", null ]
];