var namespace_book_publisher_1_1_data =
[
    [ "AUTHOR", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r.html", "class_book_publisher_1_1_data_1_1_a_u_t_h_o_r" ],
    [ "BOOK", "class_book_publisher_1_1_data_1_1_b_o_o_k.html", "class_book_publisher_1_1_data_1_1_b_o_o_k" ],
    [ "BookPublisherDBEntities1", "class_book_publisher_1_1_data_1_1_book_publisher_d_b_entities1.html", "class_book_publisher_1_1_data_1_1_book_publisher_d_b_entities1" ],
    [ "PRESS", "class_book_publisher_1_1_data_1_1_p_r_e_s_s.html", "class_book_publisher_1_1_data_1_1_p_r_e_s_s" ],
    [ "PRESS_DATA", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a.html", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a" ]
];