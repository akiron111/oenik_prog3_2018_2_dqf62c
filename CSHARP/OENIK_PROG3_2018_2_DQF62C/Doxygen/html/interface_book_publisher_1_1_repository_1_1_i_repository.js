var interface_book_publisher_1_1_repository_1_1_i_repository =
[
    [ "Delete", "interface_book_publisher_1_1_repository_1_1_i_repository.html#a5792eaa8ac0f615db1533a918bb56018", null ],
    [ "GetAllAuthor", "interface_book_publisher_1_1_repository_1_1_i_repository.html#afd24b8a924c7fb4f866b4b0b94ddf1aa", null ],
    [ "GetAllBook", "interface_book_publisher_1_1_repository_1_1_i_repository.html#a423ac7cc1348ab381556c3f1f0e6db04", null ],
    [ "GetAllPress", "interface_book_publisher_1_1_repository_1_1_i_repository.html#a45666a047d2631a3a024316eb05ca7c4", null ],
    [ "GetAllPressData", "interface_book_publisher_1_1_repository_1_1_i_repository.html#ae7d8d4bdf800b325ba3fe404e8fcaa8a", null ],
    [ "Insert", "interface_book_publisher_1_1_repository_1_1_i_repository.html#a5961b8b6e7f68fbda2343042cb60c014", null ],
    [ "Update", "interface_book_publisher_1_1_repository_1_1_i_repository.html#ab8128f4ad3b07b0250f9fb10f462db3b", null ]
];