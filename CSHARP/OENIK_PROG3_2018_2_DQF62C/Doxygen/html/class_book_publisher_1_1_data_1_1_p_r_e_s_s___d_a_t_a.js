var class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a =
[
    [ "BOOK", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a.html#a2a1d3d9cb6f8dafa448e26d27fb2000f", null ],
    [ "COPYNO", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a.html#a44588cbe2d0d87fddd7348246aed792e", null ],
    [ "DATAID", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a.html#a49e109e1de48e840866ef282872e269b", null ],
    [ "ISBN", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a.html#a344de4e453e4113df9702ae9bc7ebe4c", null ],
    [ "PAGENO", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a.html#a15b6b37df83851cd2ba16925fc1471f1", null ],
    [ "PAPERWEIGHT", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a.html#a2a6b8e2c48339576ed3cffd75395873b", null ],
    [ "PRESS", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a.html#a004ef194fd5ea6aaf41f1852b2a1c19c", null ],
    [ "PRESS_ID", "class_book_publisher_1_1_data_1_1_p_r_e_s_s___d_a_t_a.html#ade718dfb64c55b4b4b61b98f2d0ef346", null ]
];