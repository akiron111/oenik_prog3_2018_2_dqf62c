﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookPublisher.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookPublisher.Data;

    /// <summary>
    /// Repository class
    /// </summary>
    public class Repository : IRepository
    {
        private static BookPublisherDBEntities1 context;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        public Repository()
        {
            if (context == null)
            {
                context = new BookPublisherDBEntities1();
            }
        }

        /// <summary>
        /// Gets all author
        /// </summary>
        /// <returns>all author objects</returns>
        public IQueryable<AUTHOR> GetAllAuthor()
        {
            return context.AUTHOR.AsQueryable();
        }

        /// <summary>
        /// gets all book
        /// </summary>
        /// <returns>all book objects</returns>
        public IQueryable<BOOK> GetAllBook()
        {
            return context.BOOK.AsQueryable();
        }

        /// <summary>
        /// gets all press
        /// </summary>
        /// <returns>all press objects</returns>
        public IQueryable<PRESS> GetAllPress()
        {
            return context.PRESS.AsQueryable();
        }

        /// <summary>
        /// gets all press data
        /// </summary>
        /// <returns>all press data objects</returns>
        public IQueryable<PRESS_DATA> GetAllPressData()
        {
            return context.PRESS_DATA.AsQueryable();
        }

        /// <summary>
        /// inserts a new object to the database
        /// </summary>
        /// <param name="obj">object to be inserted</param>
        public void Insert(object obj)
        {
            if (obj is AUTHOR)
            {
                context.AUTHOR.Add((AUTHOR)obj);
            }
            else if (obj is BOOK)
            {
                context.BOOK.Add((BOOK)obj);
            }
            else if (obj is PRESS)
            {
                context.PRESS.Add((PRESS)obj);
            }
            else if (obj is PRESS_DATA)
            {
                context.PRESS_DATA.Add((PRESS_DATA)obj);
            }

            context.SaveChanges();
        }

        /// <summary>
        /// updates the database
        /// </summary>
        /// <param name="obj">object to be updated</param>
        /// <param name="key">identifier</param>
        public void Update(object obj, object key)
        {
            if (obj is AUTHOR)
            {
                AUTHOR a = obj as AUTHOR;
                var old = context.AUTHOR.Find(key);
                if (old != null)
                {
                    context.Entry(old).CurrentValues.SetValues(a);
                }
            }
            else if (obj is BOOK)
            {
                BOOK a = obj as BOOK;
                var old = context.BOOK.Find(key);
                if (old != null)
                {
                    context.Entry(old).CurrentValues.SetValues(a);
                }
            }
            else if (obj is PRESS)
            {
                PRESS a = obj as PRESS;
                var old = context.PRESS.Find(key);
                if (key != null)
                {
                    context.Entry(old).CurrentValues.SetValues(a);
                }
            }
            else if (obj is PRESS_DATA)
            {
                PRESS_DATA a = obj as PRESS_DATA;
                var old = context.PRESS_DATA.Find(key);
                if (key != null)
                {
                    context.Entry(old).CurrentValues.SetValues(a);
                }
            }

            context.SaveChanges();
        }

        /// <summary>
        /// deletes an object from the database
        /// </summary>
        /// <param name="obj">object to be deleted</param>
        public void Delete(object obj)
        {
            if (obj is AUTHOR)
            {
                context.AUTHOR.Remove((AUTHOR)obj);
            }
            else if (obj is BOOK)
            {
                context.BOOK.Remove((BOOK)obj);
            }
            else if (obj is PRESS)
            {
                context.PRESS.Remove((PRESS)obj);
            }
            else if (obj is PRESS_DATA)
            {
                context.PRESS_DATA.Remove((PRESS_DATA)obj);
            }

            context.SaveChanges();
        }
    }
}
