﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookPublisher.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookPublisher.Data;

    /// <summary>
    /// IRepositor interface for repository
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// To get all author objects
        /// </summary>
        /// <returns>all the author objects</returns>
        IQueryable<AUTHOR> GetAllAuthor();

        /// <summary>
        /// To get all book objects
        /// </summary>
        /// <returns>all the book objects</returns>
        IQueryable<BOOK> GetAllBook();

        /// <summary>
        /// To get all press objects
        /// </summary>
        /// <returns>all the press objects</returns>
        IQueryable<PRESS> GetAllPress();

        /// <summary>
        /// To get all press data objects
        /// </summary>
        /// <returns>all the press data objects</returns>
        IQueryable<PRESS_DATA> GetAllPressData();

        /// <summary>
        /// Inserts a new item to the database
        /// </summary>
        /// <param name="obj">object to be inserted</param>
        void Insert(object obj);

        /// <summary>
        /// Deletes an item from the database
        /// </summary>
        /// <param name="obj">object to be deleted</param>
        void Delete(object obj);

        /// <summary>
        /// Updates an item in the database
        /// </summary>
        /// <param name="obj">object to be updated</param>
        /// <param name="key">identifier</param>
        void Update(object obj, object key);
    }
}
