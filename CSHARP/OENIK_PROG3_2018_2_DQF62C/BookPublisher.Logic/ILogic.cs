﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookPublisher.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookPublisher.Data;

    /// <summary>
    /// ILogic interface
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// List containing all author objects
        /// </summary>
        /// <returns>all author objects</returns>
        List<AUTHOR> GetAllAuthor();

        /// <summary>
        /// Lists containing book objects
        /// </summary>
        /// <returns>all book objects</returns>
        List<BOOK> GetAllBook();

        /// <summary>
        /// Lists all press objects
        /// </summary>
        /// <returns>all press objects</returns>
        List<PRESS> GetAllPress();

        /// <summary>
        /// Lists all press data objects
        /// </summary>
        /// <returns>all press data objects</returns>
        List<PRESS_DATA> GetAllPressData();

        /// <summary>
        /// Inserts new object
        /// </summary>
        /// <param name="obj">object to be inserted</param>
        /// <returns>message of completed insertion</returns>
        string Insert(object obj);

        /// <summary>
        /// updates an object
        /// </summary>
        /// <param name="obj">object to be updated</param>
        /// <param name="key">identifier</param>
        /// <returns>message of completed update</returns>
        string Update(object obj, object key);

        /// <summary>
        /// deletes an object
        /// </summary>
        /// <param name="obj">object to be deleted</param>
        /// <returns>message of completed deletion</returns>
        string Delete(object obj);
    }
}
