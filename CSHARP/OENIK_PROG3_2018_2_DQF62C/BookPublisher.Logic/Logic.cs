﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookPublisher.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookPublisher.Data;
    using BookPublisher.Repository;

    /// <summary>
    /// Logic class
    /// </summary>
    public class Logic : ILogic
    {
        private readonly IRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        public Logic()
        {
            if (this.repo == null)
            {
                this.repo = new Repository();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        /// <param name="testRepo">creates a test repository instance</param>
        public Logic(IRepository testRepo)
        {
            this.repo = testRepo;
        }

        /// <summary>
        /// gets all author objects
        /// </summary>
        /// <returns>all author objects</returns>
        public List<AUTHOR> GetAllAuthor()
        {
            return this.repo.GetAllAuthor().ToList();
        }

        /// <summary>
        /// gets all book objects
        /// </summary>
        /// <returns>all book objects</returns>
        public List<BOOK> GetAllBook()
        {
            return this.repo.GetAllBook().ToList();
        }

        /// <summary>
        /// gets all press objects
        /// </summary>
        /// <returns>all press objects</returns>
        public List<PRESS> GetAllPress()
        {
            return this.repo.GetAllPress().ToList();
        }

        /// <summary>
        /// gets all press data objects
        /// </summary>
        /// <returns>all press data objects</returns>
        public List<PRESS_DATA> GetAllPressData()
        {
            return this.repo.GetAllPressData().ToList();
        }

        /// <summary>
        /// inserts a new object into the database
        /// </summary>
        /// <param name="obj">object tobe inserted</param>
        /// <returns>message os completed insertion</returns>
        public string Insert(object obj)
        {
            this.repo.Insert(obj);
            return "Felvíve.";
        }

        /// <summary>
        /// updates an object
        /// </summary>
        /// <param name="obj">object to be updated</param>
        /// <param name="key">identifier</param>
        /// <returns>messgae of completed update</returns>
        public string Update(object obj, object key)
        {
            this.repo.Update(obj, key);
            return "Módosítva.";
        }

        /// <summary>
        /// deletes an object
        /// </summary>
        /// <param name="obj">object to be deleted</param>
        /// <returns>message of completed deletion</returns>
        public string Delete(object obj)
        {
            this.repo.Delete(obj);
            return "Törölve.";
        }

        /// <summary>
        /// NonCRUD method
        /// </summary>
        /// <returns>how many books each press printed</returns>
        public List<string> PressesWithBooks()
        {
            var result = (from press in this.repo.GetAllPress()
                         join data in this.repo.GetAllPressData()
                         on press.PRESS_ID equals data.PRESS_ID
                         group press by press.PRESSNAME into g
                         select new
                         {
                             pressName = g.Key,
                             Count = g.Count()
                         }).ToList();
            return result.Select(a => a.pressName + ": " + a.Count + " db").ToList();
        }

        /// <summary>
        /// NonCRUD 2
        /// </summary>
        /// <returns>the writer of most books</returns>
        public List<string> MostBooks()
        {
            var result = (from author in this.repo.GetAllAuthor()
                          join book in this.repo.GetAllBook()
                          on author.AUTHOR_ID equals book.AUTHOR_ID
                          group author by author.NAME into g
                          select new
                          {
                              authorName = g.Key,
                              Count = g.Count()
                          }).ToList();
            return result.Where(x => x.Count == result.Max(a => a.Count)).Select(x => x.authorName + " " + x.Count + " könyvet írt").ToList();
        }

        /// <summary>
        /// NonCRUD 3
        /// </summary>
        /// <returns>how many books of an outhor were prnted in each presses</returns>
        public List<string> BooksFromPresses()
        {
            var result = (from author in this.repo.GetAllAuthor()
                          join book in this.repo.GetAllBook()
                          on author.AUTHOR_ID equals book.AUTHOR_ID
                          join data in this.repo.GetAllPressData() on book.ISBN equals data.ISBN
                          join press in this.repo.GetAllPress() on data.PRESS_ID equals press.PRESS_ID
                          group author by new { author.NAME, press.PRESSNAME } into g
                          select new
                          {
                              authorName = g.Key.NAME,
                              pressName = g.Key.PRESSNAME,
                              count = g.Count()
                          }).ToList();
            return result.Select(x => x.authorName + " szerzőnek " + x.pressName + " nyomdában " + x.count + " könyvét nyomtatták").ToList();
        }
    }
}
