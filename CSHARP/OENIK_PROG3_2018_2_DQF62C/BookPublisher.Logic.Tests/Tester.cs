﻿// <copyright file="Tester.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookPublisher.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookPublisher.Data;
    using BookPublisher.Logic;
    using BookPublisher.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Logic tester class
    /// </summary>
    [TestFixture]
    public class Tester
    {
        private Logic logic;
        private Mock<IRepository> mockRepo;

        /// <summary>
        /// sets up the mock ILogic for testing
        /// </summary>
        [SetUp]
        public void SetupLogic()
        {
            this.mockRepo = new Mock<IRepository>();

            List<AUTHOR> authors = new List<AUTHOR>()
            {
                new AUTHOR() { AUTHOR_ID = 72, ACCOUNTNO = 4123456789, ADDRESS = "Valami lane 22.", EMAIL = "valaki@gmail.com", NAME = "Andy Weir" },
                new AUTHOR() { AUTHOR_ID = 42, ACCOUNTNO = 4352672828, ADDRESS = "Valami lane 22.", EMAIL = "whatever@gmail.com", NAME = "Wahetever Szerzo" },
                new AUTHOR() { AUTHOR_ID = 52, ACCOUNTNO = 9282277272, ADDRESS = "Valami lane 22.", EMAIL = "ember@gmail.com", NAME = "Ember Szerzo" }
            };

            List<BOOK> books = new List<BOOK>()
            {
                new BOOK() { ISBN = 1123456781234, AUTHOR_ID = 72, WORDCOUNT = 50000, ADDRESS = "Artemis" },
                new BOOK() { ISBN = 4123456781234, AUTHOR_ID = 72, WORDCOUNT = 50000, ADDRESS = "The Martian" },
                new BOOK() { ISBN = 6123456781234, AUTHOR_ID = 52, WORDCOUNT = 50000, ADDRESS = "Release" }
            };

            List<PRESS> presses = new List<PRESS>()
            {
                new PRESS() { PRESS_ID = 87, PRESSNAME = "Valamilyen Nyomda", EMAIL = "valami@nyomda.hu" },
                new PRESS() { PRESS_ID = 88, PRESSNAME = "Whatever Nyomda", EMAIL = "whatever@nyomda.hu" },
                new PRESS() { PRESS_ID = 67, PRESSNAME = "Milegyen Nyomda", EMAIL = "milegyen@nyomda.hu" }
            };

            List<PRESS_DATA> data = new List<PRESS_DATA>()
            {
                new PRESS_DATA() { DATAID = 98, PRESS_ID = 87, ISBN = 1123456781234 },
                new PRESS_DATA() { DATAID = 45, PRESS_ID = 87, ISBN = 4123456781234 },
                new PRESS_DATA() { DATAID = 55, PRESS_ID = 88, ISBN = 6123456781234 }
            };

            this.mockRepo.Setup(x => x.GetAllAuthor()).Returns(authors.AsQueryable());
            this.mockRepo.Setup(x => x.GetAllBook()).Returns(books.AsQueryable());
            this.mockRepo.Setup(x => x.GetAllPress()).Returns(presses.AsQueryable());
            this.mockRepo.Setup(x => x.GetAllPressData()).Returns(data.AsQueryable());

            this.logic = new Logic(this.mockRepo.Object);
        }

        /// <summary>
        /// tests the GetAllAuthor methid from Logic
        /// </summary>
        [Test]
        public void GetAllAuthorTest()
        {
            var authors = this.logic.GetAllAuthor();
            Assert.That(authors.Count, Is.EqualTo(3));
        }

        /// <summary>
        /// tests the GetAllBook method from Logic
        /// </summary>
        [Test]
        public void GetAllBookTest()
        {
            var books = this.logic.GetAllBook();
            Assert.That(books.Count, Is.EqualTo(3));
        }

        /// <summary>
        /// tests the GetAllPress method from Logic
        /// </summary>
        [Test]
        public void GetAllPressTest()
        {
            var presses = this.logic.GetAllPress();
            Assert.That(presses.Count, Is.EqualTo(3));
        }

        /// <summary>
        /// tests the GetAllPressData method from Logic
        /// </summary>
        [Test]
        public void GetAllDataTest()
        {
            var data = this.logic.GetAllPressData();
            Assert.That(data.Count, Is.EqualTo(3));
        }

        /// <summary>
        /// tests the Delete method of Logic
        /// </summary>
        [Test]
        public void TestDeleteAuthor()
        {
            var authorToDelete = this.mockRepo.Object.GetAllAuthor().FirstOrDefault(x => x.AUTHOR_ID == 72);

            this.mockRepo.Setup(a => a.Delete(It.IsAny<AUTHOR>()));
            var result = this.logic.Delete(authorToDelete);

            this.mockRepo.Verify(a => a.Delete(authorToDelete));
            Assert.That(result, Is.EqualTo("Törölve."));
        }

        /// <summary>
        /// tests the Delete method of Logic
        /// </summary>
        [Test]
        public void TestDeleteBook()
        {
            var bookToDelete = this.logic.GetAllBook().FirstOrDefault(x => x.ISBN == 4123456781234);

            this.mockRepo.Setup(a => a.Delete(It.IsAny<AUTHOR>()));
            var result = this.logic.Delete(bookToDelete);

            this.mockRepo.Verify(a => a.Delete(bookToDelete));
            Assert.That(result, Is.EqualTo("Törölve."));
        }

        /// <summary>
        /// tests the Delete method of Logic
        /// </summary>
        [Test]
        public void TestDeletePress()
        {
            var pressToDelete = this.logic.GetAllPress().FirstOrDefault(x => x.PRESS_ID == 87);

            this.mockRepo.Setup(a => a.Delete(It.IsAny<PRESS>()));
            var result = this.logic.Delete(pressToDelete);

            this.mockRepo.Verify(a => a.Delete(pressToDelete));
            Assert.That(result, Is.EqualTo("Törölve."));
        }

        /// <summary>
        /// tests the Delete method of Logic
        /// </summary>
        [Test]
        public void TestDeletePressData()
        {
            var dataToDelete = this.logic.GetAllPressData().FirstOrDefault(x => x.PRESS_ID == 87);

            this.mockRepo.Setup(a => a.Delete(It.IsAny<PRESS_DATA>()));
            var result = this.logic.Delete(dataToDelete);

            this.mockRepo.Verify(a => a.Delete(dataToDelete));
            Assert.That(result, Is.EqualTo("Törölve."));
        }

        /// <summary>
        /// tests the Update method of Logic
        /// </summary>
        [Test]
        public void TestUpdateAuthor()
        {
            var authorToUpdate = this.logic.GetAllAuthor().FirstOrDefault(x => x.AUTHOR_ID == 72);
            authorToUpdate.NAME = authorToUpdate.NAME + " módosítva";

            this.mockRepo.Setup(a => a.Update(It.IsAny<AUTHOR>(), It.IsAny<object>()));
            var result = this.logic.Update(authorToUpdate, authorToUpdate.AUTHOR_ID);

            this.mockRepo.Verify(a => a.Update(authorToUpdate, authorToUpdate.AUTHOR_ID));
            Assert.That(result, Is.EqualTo("Módosítva."));
        }

        /// <summary>
        /// tests the Update method of Logic
        /// </summary>
        [Test]
        public void TestUpdateBook()
        {
            var bookToUpdate = this.logic.GetAllBook().FirstOrDefault(x => x.ISBN == 4123456781234);
            bookToUpdate.ADDRESS = bookToUpdate.ADDRESS + " :)";

            this.mockRepo.Setup(a => a.Update(It.IsAny<BOOK>(), It.IsAny<object>()));
            var result = this.logic.Update(bookToUpdate, bookToUpdate.ISBN);

            this.mockRepo.Verify(a => a.Update(bookToUpdate, bookToUpdate.ISBN));
            Assert.That(result, Is.EqualTo("Módosítva."));
        }

        /// <summary>
        /// tests the Update method of Logic
        /// </summary>
        [Test]
        public void TestUpdatePress()
        {
            var pressToUpdate = this.logic.GetAllPress().FirstOrDefault(x => x.PRESS_ID == 87);
            pressToUpdate.PRESSNAME = pressToUpdate.PRESSNAME + " presser";

            this.mockRepo.Setup(a => a.Update(It.IsAny<PRESS>(), It.IsAny<object>()));
            var result = this.logic.Update(pressToUpdate, pressToUpdate.PRESS_ID);

            this.mockRepo.Verify(a => a.Update(pressToUpdate, pressToUpdate.PRESS_ID));
            Assert.That(result, Is.EqualTo("Módosítva."));
        }

        /// <summary>
        /// tests the Update method of Logic
        /// </summary>
        [Test]
        public void TestUpdatePressData()
        {
            var dataToUpdate = this.logic.GetAllPressData().FirstOrDefault(x => x.PRESS_ID == 87);
            dataToUpdate.PAPERWEIGHT = dataToUpdate.PAPERWEIGHT + 1;

            this.mockRepo.Setup(a => a.Update(It.IsAny<PRESS_DATA>(), It.IsAny<object>()));
            var result = this.logic.Update(dataToUpdate, dataToUpdate.PRESS_ID);

            this.mockRepo.Verify(a => a.Update(dataToUpdate, dataToUpdate.PRESS_ID));
            Assert.That(result, Is.EqualTo("Módosítva."));
        }

        /// <summary>
        /// tests the Insert method of Logic
        /// </summary>
        [Test]
        public void TestInsertAuthor()
        {
            AUTHOR newAuthor = new AUTHOR()
            {
                AUTHOR_ID = 76,
                ACCOUNTNO = 54637388282,
                ADDRESS = "Valami street 25.",
                EMAIL = "whatever@gmail.com",
                NAME = "Whatever Author"
            };

            this.mockRepo.Setup(a => a.Insert(It.IsAny<AUTHOR>()));
            var result = this.logic.Insert(newAuthor);

            this.mockRepo.Verify(a => a.Insert(newAuthor));
            Assert.That(result, Is.EqualTo("Felvíve."));
        }

        /// <summary>
        /// tests the Insert method of Logic
        /// </summary>
        [Test]
        public void TestInsertBook()
        {
            BOOK newBook = new BOOK()
            {
                ISBN = 7664738975635,
                AUTHOR_ID = 76,
                WORDCOUNT = 5000,
                ADDRESS = "Artemis"
            };

            this.mockRepo.Setup(a => a.Insert(It.IsAny<BOOK>()));
            var result = this.logic.Insert(newBook);

            this.mockRepo.Verify(a => a.Insert(newBook));
            Assert.That(result, Is.EqualTo("Felvíve."));
        }

        /// <summary>
        /// tests the Insert method of Logic
        /// </summary>
        [Test]
        public void TestInsertPress()
        {
            PRESS newPress = new PRESS()
            {
                PRESS_ID = 13,
                PRESSNAME = "Uj Nyomda Kft",
                EMAIL = "uj.nyomda@gmail.com"
            };

            this.mockRepo.Setup(a => a.Insert(It.IsAny<PRESS>()));
            var result = this.logic.Insert(newPress);

            this.mockRepo.Verify(a => a.Insert(newPress));
            Assert.That(result, Is.EqualTo("Felvíve."));
        }

        /// <summary>
        /// tests the Insert method of Logic
        /// </summary>
        [Test]
        public void TestInsertData()
        {
            PRESS_DATA newData = new PRESS_DATA()
            {
                DATAID = 4,
                PRESS_ID = 13,
                ISBN = 746375838746,
                COPYNO = 2000,
                PAGENO = 367,
                PAPERWEIGHT = 150
            };

            this.mockRepo.Setup(a => a.Insert(It.IsAny<PRESS_DATA>()));
            var result = this.logic.Insert(newData);

            this.mockRepo.Verify(a => a.Insert(newData));
            Assert.That(result, Is.EqualTo("Felvíve."));
        }

        /// <summary>
        /// tests the first NonCRUD method of Logic
        /// </summary>
        [Test]
        public void TestNonCRUD1()
        {
            List<string> res1 = this.logic.PressesWithBooks();

            Assert.That(res1.ElementAt(0), Is.EqualTo("Valamilyen Nyomda: 2 db"));
            Assert.That(res1.ElementAt(1), Is.EqualTo("Whatever Nyomda: 1 db"));
        }

        /// <summary>
        /// tests the second NonCRUD method of Logic
        /// mely szerzők írták a legtöbb könyvet
        /// </summary>
        [Test]
        public void TestNonCRUD2()
        {
            var res = this.logic.MostBooks();

            Assert.That(res.ElementAt(0), Is.EqualTo("Andy Weir 2 könyvet írt"));
        }

        /// <summary>
        /// tests the third NonCRUD method of Logic
        /// </summary>
        [Test]
        public void TestNonCRUD3()
        {
            var res = this.logic.BooksFromPresses();

            Assert.That(res.ElementAt(0), Is.EqualTo("Andy Weir szerzőnek Valamilyen Nyomda nyomdában 2 könyvét nyomtatták"));
            Assert.That(res.ElementAt(1), Is.EqualTo("Ember Szerzo szerzőnek Whatever Nyomda nyomdában 1 könyvét nyomtatták"));
        }
    }
}
