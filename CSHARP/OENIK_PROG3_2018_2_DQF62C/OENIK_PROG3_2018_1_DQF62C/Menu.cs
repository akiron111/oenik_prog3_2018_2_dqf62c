﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG3_2018_1_DQF62C
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using BookPublisher.Data;
    using BookPublisher.Logic;

    /// <summary>
    /// Class of main console menu
    /// </summary>
    public class Menu
    {
        private static Logic logic;

        /// <summary>
        /// writes the menu to the console
        /// </summary>
        public static void ShowMenu()
        {
            logic = new Logic();

            ConsoleKeyInfo cki;

            do
            {
                Console.WriteLine("Válassz menüpontot: ");
                Console.WriteLine("1: elemek listázása");
                Console.WriteLine("2: elem hozzáadása");
                Console.WriteLine("3: elem módosítása");
                Console.WriteLine("4: elem törlése");
                Console.WriteLine("5: Java elérés");
                Console.WriteLine("6: Nyomdák által nyomtatott könyvek");
                Console.WriteLine("7: Mely szerzők írták a legtöbb könyvet?");
                Console.WriteLine("8: Könyvek nyomdák szerinti csoportosítása");
                Console.WriteLine("\n");
                cki = Console.ReadKey(false);
                Console.Clear();
                switch (cki.KeyChar.ToString())
                {
                    case "1":
                        ListingMenu();
                        break;
                    case "2":
                        AddItemMenu();
                        break;
                    case "3":
                        UpdateItemMenu();
                        break;
                    case "4":
                        DeleteItemMenu();
                        break;
                    case "5":
                        HandleJava();
                        break;
                    case "6":
                        NonCRUD1();
                        break;
                    case "7":
                        NonCRUD2();
                        break;
                    case "8":
                        NonCRUD3();
                        break;
                    default: Console.WriteLine("Válassz valid opciót!");
                        break;
                }
            }
            while (cki.Key != ConsoleKey.Escape);
        }

        /// <summary>
        /// Lists the contents of the database
        /// </summary>
        public static void ListingMenu()
        {
            ConsoleKeyInfo cki;

            do
            {
                Console.WriteLine("A főmenübe való visszalépéshez nyomj escape-et!\n");
                Console.WriteLine("Válassz táblát:\n");
                Console.WriteLine("1: Szerző");
                Console.WriteLine("2: Könyv");
                Console.WriteLine("3: Nyomda");
                Console.WriteLine("4: Nyomdai adatok");
                Console.WriteLine("\n");
                cki = Console.ReadKey(false);
                switch (cki.KeyChar.ToString())
                {
                    case "1":
                        Listing(logic.GetAllAuthor().Cast<object>().ToList());
                        break;
                    case "2":
                        Listing(logic.GetAllBook().Cast<object>().ToList());
                        break;
                    case "3":
                        Listing(logic.GetAllPress().Cast<object>().ToList());
                        break;
                    case "4":
                        Listing(logic.GetAllPressData().Cast<object>().ToList());
                        break;
                    default:
                        Console.WriteLine("válassz valid opciót!");
                        break;
                }
            }
            while (cki.Key != ConsoleKey.Escape);
        }

        /// <summary>
        /// sonsole formatting
        /// </summary>
        /// <param name="list">list of objects to display</param>
        public static void Listing(List<object> list)
        {
            foreach (var item in list)
            {
                if (item is AUTHOR)
                {
                    AUTHOR a = item as AUTHOR;
                    Console.WriteLine("Szerző: " + a.NAME + ",\n azonosítója: " + a.AUTHOR_ID + ",\n címe: " + a.ADDRESS + ",\n születési ideje: " + a.BIRTHDATE + ",\n számlaszáma: " + a.ACCOUNTNO + ",\n email címe: " + a.EMAIL);
                    SplittingLine();
                }
                else if (item is BOOK)
                {
                    BOOK b = item as BOOK;
                    Console.WriteLine("Cím: " + b.ADDRESS + ",\n szerző azonosítója: " + b.AUTHOR_ID + ",\n ISBN: " + b.ISBN + ",\n szavak száma: " + b.WORDCOUNT);
                    SplittingLine();
                }
                else if (item is PRESS)
                {
                    PRESS p = item as PRESS;
                    Console.WriteLine("Nyomda azonosítója: " + p.PRESS_ID + ",\n nyomda neve: " + p.PRESSNAME + ",\n email címe: " + p.EMAIL);
                    SplittingLine();
                }
                else if (item is PRESS_DATA)
                {
                    PRESS_DATA pd = item as PRESS_DATA;

                    Console.WriteLine("NyomdaID: " + pd.PRESS_ID + ",\n ISBN: " + pd.ISBN + ",\n Példányszám: " + pd.COPYNO + ",\n Oldalszám: " + pd.PAGENO + ",\n papír súlya: " + pd.PAPERWEIGHT);
                    SplittingLine();
                }
                else if (item is string)
                {
                    Console.WriteLine(item + "\n");
                    SplittingLine();
                }
            }
        }

        /// <summary>
        /// Calls the insert method from the Logic class
        /// </summary>
        public static void AddItemMenu()
        {
            ConsoleKeyInfo cki;
            Console.Clear();

            do
            {
                Console.WriteLine("A főmenübe való visszalépéshez nyomj escape-et!\n");
                Console.WriteLine("Válassz táblát:\n");
                Console.WriteLine("1: Szerző");
                Console.WriteLine("2: Könyv");
                Console.WriteLine("3: Nyomda");
                Console.WriteLine("4: Nyomdai adatok");
                Console.WriteLine("\n");
                cki = Console.ReadKey(false);
                switch (cki.KeyChar.ToString())
                {
                    case "1":
                        AUTHOR author = new AUTHOR();

                        Console.WriteLine("Add meg a szerző ID-ját:");
                        author.AUTHOR_ID = int.Parse(Console.ReadLine());

                        Console.WriteLine("Add meg a szerző nevét: ");
                        author.NAME = Console.ReadLine();

                        Console.WriteLine("Add meg a szerző születési évét: ");
                        author.BIRTHDATE = Convert.ToDateTime(Console.ReadLine());

                        Console.WriteLine("Add meg a szerző címér: ");
                        author.ADDRESS = Console.ReadLine();

                        Console.WriteLine("Add meg a szerző email címét: ");
                        author.EMAIL = Console.ReadLine();

                        Console.WriteLine("Add meg a szerző számlaszámát: ");
                        author.ACCOUNTNO = int.Parse(Console.ReadLine());

                        logic.Insert(author);

                        break;
                    case "2":
                        BOOK book = new BOOK();

                        Console.WriteLine("Add meg a könyv ISBN-jét: ");
                        book.ISBN = int.Parse(Console.ReadLine());

                        Console.WriteLine("Add meg a könyvhöz tartozó szerző azonosítóját: ");
                        book.AUTHOR_ID = int.Parse(Console.ReadLine());

                        Console.WriteLine("Add meg a könyv szószámát: ");
                        book.WORDCOUNT = int.Parse(Console.ReadLine());

                        Console.WriteLine("Add meg a könyv címét: ");
                        book.ADDRESS = Console.ReadLine();

                        logic.Insert(book);

                        break;
                    case "3":
                        PRESS press = new PRESS();

                        Console.WriteLine("Add meg a nyomda ID-ját: ");
                        press.PRESS_ID = int.Parse(Console.ReadLine());

                        Console.WriteLine("Add meg a nyomda nevét: ");
                        press.PRESSNAME = Console.ReadLine();

                        Console.WriteLine("Add meg a nyomda email címét: ");
                        press.EMAIL = Console.ReadLine();

                        logic.Insert(press);
                        break;
                    case "4":
                        PRESS_DATA pressData = new PRESS_DATA();

                        Console.WriteLine("Add meg a nyomdai adatok ID-ját: ");
                        pressData.DATAID = int.Parse(Console.ReadLine());

                        Console.WriteLine("Add meg az datokhoz tartozó nyomda azonosítóját: ");
                        pressData.PRESS_ID = int.Parse(Console.ReadLine());

                        Console.WriteLine("Add meg a könyv ISBN-jét, amihez az adatok tartoznak: ");
                        pressData.ISBN = int.Parse(Console.ReadLine());

                        Console.WriteLine("Add meg a könyv darabszámát: ");
                        pressData.COPYNO = int.Parse(Console.ReadLine());

                        Console.WriteLine("Add meg a könyv oldalszámát: ");
                        pressData.PAGENO = int.Parse(Console.ReadLine());

                        Console.WriteLine("Add meg a papír súlyát: ");
                        pressData.PAPERWEIGHT = int.Parse(Console.ReadLine());

                        logic.Insert(pressData);
                        break;
                    default:
                        Console.WriteLine("Válassz valid opciót!");
                        break;
                }
            }
            while (cki.Key != ConsoleKey.Escape);
        }

        /// <summary>
        /// calls the update method from the Logic class
        /// </summary>
        public static void UpdateItemMenu()
        {
            ConsoleKeyInfo cki;

            do
            {
                Console.WriteLine("A főmenübe való visszalépéshez nyomj escape-et!\n");
                Console.WriteLine("Válassz táblát:\n");
                Console.WriteLine("1: Szerző");
                Console.WriteLine("2: Könyv");
                Console.WriteLine("3: Nyomda");
                Console.WriteLine("4: Nyomdai adatok");
                Console.WriteLine("\n");
                cki = Console.ReadKey(false);
                switch (cki.KeyChar.ToString())
                {
                    case "1":
                        Console.WriteLine("Válassz szerzőt és add meg a sorszámát:");
                        List<AUTHOR> authors = logic.GetAllAuthor().ToList();
                        int i = 1;
                        authors.ForEach(x =>
                        {
                            Console.WriteLine(i + ":{0}\n", x.NAME);
                            i++;
                        });
                        int index = int.Parse(Console.ReadLine());
                        AUTHOR newAuthor = authors.ElementAt(index - 1);

                        Console.WriteLine("Mit szeretnél módosítani?");
                        Console.WriteLine("1: név");
                        Console.WriteLine("2: születésnap");
                        Console.WriteLine("3: lakcím");
                        Console.WriteLine("4: email cím");
                        Console.WriteLine("5: számlaszám");
                        int answer = int.Parse(Console.ReadLine());

                        if (answer == 1)
                        {
                            Console.WriteLine("Add meg az új nevet:");
                            string newName = Console.ReadLine();
                            newAuthor.NAME = newName;
                            logic.Update(newAuthor, newAuthor.AUTHOR_ID);
                        }
                        else if (answer == 2)
                        {
                            Console.WriteLine("Add meg az új születésnapot");
                            var newDate = DateTime.Parse(Console.ReadLine());
                            newAuthor.BIRTHDATE = newDate;
                            logic.Update(newAuthor, newAuthor.AUTHOR_ID);
                        }
                        else if (answer == 3)
                        {
                            Console.WriteLine("Add meg az új lakcímet:");
                            string newAddress = Console.ReadLine();
                            newAuthor.ADDRESS = newAddress;
                            logic.Update(newAuthor, newAuthor.AUTHOR_ID);
                        }
                        else if (answer == 4)
                        {
                            Console.WriteLine("Add meg az új email címet!");
                            string newEmail = Console.ReadLine();
                            newAuthor.EMAIL = newEmail;
                            logic.Update(newAuthor, newAuthor.AUTHOR_ID);
                        }
                        else if (answer == 5)
                        {
                            Console.WriteLine("Add meg az új számlaszámot:");
                            int newAccNO = int.Parse(Console.ReadLine());
                            newAuthor.ACCOUNTNO = newAccNO;
                            logic.Update(newAuthor, newAuthor.AUTHOR_ID);
                        }
                        else
                        {
                            Console.WriteLine("Ilyen lehetőség nem létezik!");
                        }

                        break;
                    case "2":
                        Console.WriteLine("Válassz könyvet és add meg a sorszámát:");
                        List<BOOK> books = logic.GetAllBook().ToList();
                        int j = 1;
                        books.ForEach(x =>
                        {
                            Console.WriteLine(j + ":{0}\n", x.ADDRESS);
                            j++;
                        });
                        int index2 = int.Parse(Console.ReadLine());
                        BOOK newBook = books.ElementAt(index2 - 1);

                        Console.WriteLine("Mit szeretnél módosítani?");
                        Console.WriteLine("1: cím");
                        Console.WriteLine("2: szószám");
                        Console.WriteLine("3: írő ID-ja");
                        int answer2 = int.Parse(Console.ReadLine());

                        if (answer2 == 1)
                        {
                            Console.WriteLine("Add meg az új címet:");
                            string newTitle = Console.ReadLine();
                            newBook.ADDRESS = newTitle;
                            logic.Update(newBook, newBook.ADDRESS);
                        }
                        else if (answer2 == 2)
                        {
                            Console.WriteLine("Add meg az új szószámot");
                            var newWordCount = int.Parse(Console.ReadLine());
                            newBook.WORDCOUNT = newWordCount;
                            logic.Update(newBook, newBook.WORDCOUNT);
                        }
                        else if (answer2 == 3)
                        {
                            Console.WriteLine("Add meg az új szószámot");
                            var newAuthID = int.Parse(Console.ReadLine());
                            newBook.AUTHOR_ID = newAuthID;
                            logic.Update(newBook, newBook.AUTHOR_ID);
                        }
                        else
                        {
                            Console.WriteLine("Ilyen lehetőség nem létezik!");
                        }

                        break;

                    case "3":
                        Console.WriteLine("Válassz nyomdát és add meg a sorszámát:");
                        List<PRESS> presses = logic.GetAllPress().ToList();
                        int k = 1;
                        presses.ForEach(x =>
                        {
                            Console.WriteLine(k + ":{0}\n", x.PRESSNAME);
                            k++;
                        });
                        int index3 = int.Parse(Console.ReadLine());
                        PRESS newPress = presses.ElementAt(index3 - 1);

                        Console.WriteLine("Mit szeretnél módosítani?");
                        Console.WriteLine("1: név");
                        Console.WriteLine("2: email cím");

                        int answer3 = int.Parse(Console.ReadLine());

                        if (answer3 == 1)
                        {
                            Console.WriteLine("Add meg az új nyomda nevet:");
                            string newPressName = Console.ReadLine();
                            newPress.PRESSNAME = newPressName;
                            logic.Update(newPress, newPress.PRESSNAME);
                        }
                        else if (answer3 == 2)
                        {
                            Console.WriteLine("Add meg az új email címet");
                            var newPressEmail = Console.ReadLine();
                            newPress.EMAIL = newPressEmail;
                            logic.Update(newPress, newPress.EMAIL);
                        }
                        else
                        {
                            Console.WriteLine("Ilyen lehetőség nem létezik!");
                        }

                        break;
                    case "4":
                        Console.WriteLine("Válassz adatot és add meg a sorszámát:");
                        List<PRESS_DATA> data = logic.GetAllPressData().ToList();
                        int l = 1;
                        data.ForEach(x =>
                        {
                            Console.WriteLine(l + ":{0}\n", x.DATAID);
                            l++;
                        });
                        int index4 = int.Parse(Console.ReadLine());
                        PRESS_DATA newPressData = data.ElementAt(index4 - 1);

                        Console.WriteLine("Mit szeretnél módosítani?");
                        Console.WriteLine("1: nyomda azonosítója");
                        Console.WriteLine("2: könyv ISBN-je");
                        Console.WriteLine("3: példányszám");
                        Console.WriteLine("4: oldalszám");
                        Console.WriteLine("5: papír súlya");
                        int answer4 = int.Parse(Console.ReadLine());

                        if (answer4 == 1)
                        {
                            Console.WriteLine("Add meg az új nyomdai azonosítót:");
                            int newPressID = int.Parse(Console.ReadLine());
                            newPressData.PRESS_ID = newPressID;
                            logic.Update(newPressData, newPressData.PRESS_ID);
                        }
                        else if (answer4 == 2)
                        {
                            Console.WriteLine("Add meg az új ISBN-t:");
                            int newISBN = int.Parse(Console.ReadLine());
                            newPressData.ISBN = newISBN;
                            logic.Update(newPressData, newPressData.ISBN);
                        }
                        else if (answer4 == 3)
                        {
                            Console.WriteLine("Add meg az új példányszámot:");
                            int newCopyNo = int.Parse(Console.ReadLine());
                            newPressData.COPYNO = newCopyNo;
                            logic.Update(newPressData, newPressData.COPYNO);
                        }
                        else if (answer4 == 4)
                        {
                            Console.WriteLine("Add meg az új oldalszámot:");
                            int newPageNo = int.Parse(Console.ReadLine());
                            newPressData.PAGENO = newPageNo;
                            logic.Update(newPressData, newPressData.PAGENO);
                        }
                        else if (answer4 == 5)
                        {
                            Console.WriteLine("Add meg az új papír súlyát:");
                            int newPaperWeight = int.Parse(Console.ReadLine());
                            newPressData.PAPERWEIGHT = newPaperWeight;
                            logic.Update(newPressData, newPressData.PAPERWEIGHT);
                        }
                        else
                        {
                            Console.WriteLine("Ilyen lehetőség nem létezik!");
                        }

                        break;
                }
            }
            while (cki.Key != ConsoleKey.Escape);
        }

        /// <summary>
        /// calls the delete methos from the Logic class
        /// </summary>
        public static void DeleteItemMenu()
        {
            ConsoleKeyInfo cki;

            do
            {
                Console.WriteLine("A főmenübe való visszalépéshez nyomj escape-et!\n");
                Console.WriteLine("Válassz táblát:\n");
                Console.WriteLine("1: Szerző");
                Console.WriteLine("2: Könyv");
                Console.WriteLine("3: Nyomda");
                Console.WriteLine("4: Nyomdai adatok");
                Console.WriteLine("\n");
                cki = Console.ReadKey(false);
                switch (cki.KeyChar.ToString())
                {
                    case "1":
                        Console.WriteLine("Válassz törlendő szerzőt és add meg a sorszámát:");
                        List<AUTHOR> authors = logic.GetAllAuthor().ToList();
                        int i = 1;
                        authors.ForEach(x =>
                        {
                            Console.WriteLine(i + ":{0}\n", x.NAME);
                            i++;
                        });
                        int index = int.Parse(Console.ReadLine());
                        AUTHOR newAuthor = authors.ElementAt(index - 1);
                        logic.Delete(newAuthor);
                        break;
                    case "2":
                        Console.WriteLine("Válassz törlendő könyvet és add meg a sorszámát:");
                        List<BOOK> books = logic.GetAllBook().ToList();
                        int j = 1;
                        books.ForEach(x =>
                        {
                            Console.WriteLine(j + ":{0}\n", x.ADDRESS);
                            j++;
                        });
                        int index2 = int.Parse(Console.ReadLine());
                        BOOK newBook = books.ElementAt(index2 - 1);
                        logic.Delete(newBook);
                        break;
                    case "3":
                        Console.WriteLine("Válassz törlendő nyomdát és add meg a sorszámát:");
                        List<PRESS> presses = logic.GetAllPress().ToList();
                        int k = 1;
                        presses.ForEach(x =>
                        {
                            Console.WriteLine(k + ":{0}\n", x.PRESSNAME);
                            k++;
                        });
                        int index3 = int.Parse(Console.ReadLine());
                        PRESS newPress = presses.ElementAt(index3 - 1);
                        logic.Delete(newPress);
                        break;
                    case "4":
                        Console.WriteLine("Válassz törlendő nyomdai adatokat és add meg a sorszámát:");
                        List<PRESS_DATA> data = logic.GetAllPressData().ToList();
                        int l = 1;
                        data.ForEach(x =>
                        {
                            Console.WriteLine(l + ":{0}\n", x.DATAID);
                            l++;
                        });
                        int index4 = int.Parse(Console.ReadLine());
                        PRESS_DATA newData = data.ElementAt(index4 - 1);
                        logic.Delete(newData);
                    break;
                }
            }
            while (cki.Key != ConsoleKey.Escape);
        }

        /// <summary>
        /// handles java endpoint
        /// </summary>
        public static void HandleJava()
        {
            Console.WriteLine("Milyen hosszú könyvet szeretnél? Adj meg egy szószámot: ");
            int y = int.Parse(Console.ReadLine());
            XDocument xdoc = XDocument.Load($"http://localhost:8080/OENIK_PROG3_2018_2_DQF62C_JAVA/Words?words={y}");
            var x = xdoc.Descendants("data").Select(node => new
            {
                TITLE = node.Element("title")?.Value,
                WORDS = node.Element("words")?.Value.Split('.')[0]
            }).ToList();

            foreach (var item in x)
            {
                Console.WriteLine($" Cím:  {item.TITLE}  \n Szavak száma: {item.WORDS} \n");
                SplittingLine();
            }
        }

        /// <summary>
        /// calls the first NonCRUD method from Logic
        /// </summary>
        public static void NonCRUD1()
        {
            Listing(logic.PressesWithBooks().Cast<object>().ToList());
        }

        /// <summary>
        /// calls the second NonCRUD method from Logic
        /// </summary>
        public static void NonCRUD2()
        {
            Listing(logic.MostBooks().Cast<object>().ToList());
        }

        /// <summary>
        /// calls the third NonCRUD method from Logic
        /// </summary>
        public static void NonCRUD3()
        {
            Listing(logic.BooksFromPresses().Cast<object>().ToList());
        }

        /// <summary>
        /// writes a line that splits the content on the console
        /// </summary>
        public static void SplittingLine()
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("-----------------------------------");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
