/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Osztaly.Book;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kiro9
 */
@XmlRootElement()
public class BooksWordCount {
    @XmlElement
    List<Book> data;

    public BooksWordCount() {
    }
    
    public BooksWordCount(int original) {
        this.data = BooksWordCount(original);
    }
    
    private List<Book> BooksWordCount(double original){
        ArrayList<Book> local = new ArrayList<>();

        for (int i = 1; i < 6; i+=1) {
            Book p1 = new Book((int) original, i);
            local.add(p1);
        }
        return local;
    }
    
}
