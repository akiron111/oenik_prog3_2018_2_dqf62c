/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Osztaly;

import java.util.Random;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kiro9
 */
@XmlRootElement
public class Book {
    private double modified;

    @XmlElement(name = "title")
    public String getTitle() {
        return title;
    }
    private String title;
    Random r = new Random();
    
    public Book() {
    }

    public Book(int original, int rate_num) {
        double x = (Math.random()*((500-50)+1))+50;
        int y = r.nextInt((15 - 0) + 1) + 0;
        this.modified = original + x;
        this.title = titles[y];
    }

    @XmlElement(name = "words")
    public double getModified() {
        return modified;
    }

    public void setModified(double modified) {
        this.modified = modified;
    }
    
    String [] titles =
    {
       "Komfortos Mennyország",
       "The Martian",
       "Egy gésa emlékiratai",
       "Felhőatlasz",
       "Viharsziget",
       "Az élet, a világmindenség, meg minden",
       "Vendéglő a világ végén",
       "Viszlát és kösz a halakat",
       "Galaxis Útikalauz Stopposoknak",
       "Állatfarm",
       "1984",
       "Az írnok és a fáraó",
       "Felszáll a köd",
       "Dorian Gray arcképe",
       "Szólít a szörny",
       "More Than This",
       "451 Fahrenheit"
    };
    
}
